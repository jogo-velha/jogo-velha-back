package com.itau.jogovelha.controller;

import com.itau.jogovelha.model.Jogada;
import com.itau.jogovelha.model.Placar;
import com.itau.jogovelha.model.Rodada;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.itau.jogovelha.model.Tabuleiro;

@Controller
@CrossOrigin
public class JogoController {

	Rodada rodada = new Rodada("Jogador 1", "Jogador 2");

	@RequestMapping("/")
	public @ResponseBody
	Placar getPlacar() {
		jogoServicoPost("Funcao - getPlacar = " + rodada);
		return rodada.getPlacar();
	}

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public @ResponseBody
    Placar jogar(@RequestBody Jogada jogada) {
        rodada.jogar(jogada.x, jogada.y);
		jogoServicoPost("Funcao - Jogar = " + rodada);
        return rodada.getPlacar();
    }

    @RequestMapping("/iniciar")
    public @ResponseBody
    Placar iniciarJogo() {
        rodada.iniciarJogo();
		jogoServicoPost("Funcao - iniciar = " + rodada);
        return rodada.getPlacar();
    }
    
    
    
    public void jogoServicoPost(String mensagem) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			String teste = restTemplate.postForObject("http://localhost:8082/send", mensagem, String.class);
			System.out.println(teste);
		} 		
		catch (HttpClientErrorException e) {
			System.out.println(e.getResponseBodyAsString());
		}
		catch(Exception e)

		{
			System.out.println(e.getMessage());
		}
	}

}
